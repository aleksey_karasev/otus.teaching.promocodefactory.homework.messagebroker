﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Options;
using RabbitMQ.Client;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Async
{
    public class GivingPromoCodeToCustomerRabbitMQ
        : IGivingPromoCodeToCustomerGateway
    {
        private readonly string _exchange = "GivingPromoCodeToCustomer.Exchange";

        private readonly Dictionary<Type, string> _routingKey = new Dictionary<Type, string>()
        {
            {typeof(PromoCode), "GivingPromoCodeToCustomer.PromoCode" }
        };

        private readonly ConnectionFactory _connectionFactory;

        public GivingPromoCodeToCustomerRabbitMQ(IOptions<RabbitSettings> rabbitSettings)
        {
            _connectionFactory = new ConnectionFactory()
            {
                UserName = rabbitSettings.Value.User,
                Password = rabbitSettings.Value.Password,
                Port = 5672,
                HostName = rabbitSettings.Value.Server,
                VirtualHost = rabbitSettings.Value.VirtualHost
            };

            using var con = _connectionFactory.CreateConnection();

            if (con.IsOpen)
            {
                using var channel = con.CreateModel();

                channel.ExchangeDeclare(
                    exchange: _exchange,
                    type: ExchangeType.Direct,
                    durable: true);
            }
        }

        public async Task GivePromoCodeToCustomer(PromoCode promoCode)
        {
            using var con = _connectionFactory.CreateConnection();

            if (con.IsOpen)
            {
                using var channel = con.CreateModel();

                var dto = new GivePromoCodeToCustomerDto()
                {
                    PartnerId = promoCode.Partner.Id,
                    BeginDate = promoCode.BeginDate.ToShortDateString(),
                    EndDate = promoCode.EndDate.ToShortDateString(),
                    PreferenceId = promoCode.PreferenceId,
                    PromoCode = promoCode.Code,
                    ServiceInfo = promoCode.ServiceInfo,
                    PartnerManagerId = promoCode.PartnerManagerId
                };

                var body = JsonSerializer.Serialize(dto);
                var bytes = Encoding.UTF8.GetBytes(body);

                channel.BasicPublish(
                    exchange: _exchange,
                    routingKey: _routingKey[promoCode.GetType()],
                    body: bytes);
            }
        }
    }
}