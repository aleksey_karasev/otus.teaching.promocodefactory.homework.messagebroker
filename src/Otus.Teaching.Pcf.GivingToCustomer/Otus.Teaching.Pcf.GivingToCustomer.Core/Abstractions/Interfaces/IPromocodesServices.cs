﻿
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.DTOs;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Interfaces
{
    public interface IPromocodesServices
    {
        public Task<bool> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeDTO request);
    }
}
