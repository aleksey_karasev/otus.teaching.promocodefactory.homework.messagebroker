﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.DTOs;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Interfaces;
using Otus.Teaching.Pcf.GivingToCustomer.Integration.Dto;
using Otus.Teaching.Pcf.GivingToCustomer.Integration.Options;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Integration
{
    public class ConsumerGivingToCustomer : BackgroundService
    {
        private readonly string _exchange = "GivingPromoCodeToCustomer.Exchange";
        private readonly string _queue = "GivingPromoCodeToCustomer.Queue";

        private readonly IServiceScopeFactory _serivceFactory;

        private readonly string[] _routingKey = new string[]
        {
            "GivingPromoCodeToCustomer.PromoCode"
        };

        private readonly ConnectionFactory _connectionFactory;

        public ConsumerGivingToCustomer(IOptions<RabbitSettings> rabbitSettings, IServiceScopeFactory serivceFactory)
        {
            _serivceFactory = serivceFactory;

            _connectionFactory = new ConnectionFactory()
            {
                UserName = rabbitSettings.Value.User,
                Password = rabbitSettings.Value.Password,
                Port = 5672,
                HostName = rabbitSettings.Value.Server,
                VirtualHost = rabbitSettings.Value.VirtualHost
            };
        }


        private IModel InitConsmer()
        {
            var con = _connectionFactory.CreateConnection();

            var channel = con.CreateModel();

            channel.QueueDeclare(
                queue: _queue,
                exclusive: false,
                durable: true);

            foreach (var key in _routingKey)
            {
                channel.QueueBind(
                    queue: _queue,
                    exchange: _exchange,
                    routingKey: key);
            }

            return channel;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var _channel = InitConsmer();
            var consumer = new EventingBasicConsumer(_channel);

            consumer.Received += (s, data) =>
            {
                var str = Encoding.UTF8.GetString(data.Body.ToArray());

                var dataDTO = JsonSerializer.Deserialize<GivePromoCodeDTO>(str);

                GivePromoCodes(dataDTO);

                _channel.BasicAck(data.DeliveryTag, false);
            };
            _channel.BasicConsume(queue: _queue, autoAck: false, consumer: consumer);

            return Task.CompletedTask;
        }

        private void GivePromoCodes(GivePromoCodeDTO dto)
        {
            using (var scope = _serivceFactory.CreateScope())
            {
                var promocodesService = scope.ServiceProvider.GetService<IPromocodesServices>();

                promocodesService.GivePromoCodesToCustomersWithPreferenceAsync(dto);
            }
        }
    }

}