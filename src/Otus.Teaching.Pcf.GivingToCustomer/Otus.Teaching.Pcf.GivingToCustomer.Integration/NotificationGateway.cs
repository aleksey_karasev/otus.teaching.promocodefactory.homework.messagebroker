﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.GivingToCustomer.Integration.Options;
using RabbitMQ.Client;

namespace Otus.Teaching.Pcf.GivingToCustomer.Integration
{
    public class NotificationGateway
        : INotificationGateway
    {

        public async Task SendNotificationToPartnerAsync(Guid partnerId, string message)
        {
            //Код, который вызывает сервис отправки уведомлений партнеру
        }

    }
}