﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.DTOs;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Interfaces;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IPromocodesServices _promocodesServices;

        public PromocodesController(IRepository<PromoCode> promoCodesRepository, IPromocodesServices promocodesServices)
        {
            _promoCodesRepository = promoCodesRepository;
            _promocodesServices = promocodesServices;
        }
        
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promoCodesRepository.GetAllAsync();

            var response = promocodes.Select(x => new PromoCodeShortResponse()
            {
                Id = x.Id,
                Code = x.Code,
                BeginDate = x.BeginDate.ToString("yyyy-MM-dd"),
                EndDate = x.EndDate.ToString("yyyy-MM-dd"),
                PartnerId = x.PartnerId,
                ServiceInfo = x.ServiceInfo
            }).ToList();

            return Ok(response);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            GivePromoCodeDTO dto = new GivePromoCodeDTO()
            {
                BeginDate = request.BeginDate,
                EndDate = request.EndDate,
                PartnerId= request.PartnerId,
                PreferenceId= request.PreferenceId,
                PromoCode = request.PromoCode, 
                PromoCodeId = request.PromoCodeId,
                ServiceInfo = request.ServiceInfo   
            };

            await _promocodesServices.GivePromoCodesToCustomersWithPreferenceAsync(dto);

            return CreatedAtAction(nameof(GetPromocodesAsync), new { }, null);
        }
    }
}